import 'package:flutter/material.dart';

import '../constants/app_colors.dart';
import '../constants/app_styles.dart';

class AppAppBar extends StatelessWidget implements PreferredSizeWidget {
  const AppAppBar({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: title == null
          ? null
          : Text(
              title!,
              style: AppStyles.s20w500,
            ),
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      foregroundColor: AppColors.mainText,
      elevation: 0.0,
    );
  }
}
