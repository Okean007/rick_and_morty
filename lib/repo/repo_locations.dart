// ignore_for_file: avoid_print

import 'package:flutter_app_1/dto/location.dart';
import 'package:flutter_app_1/generated/l10n.dart';
import 'package:flutter_app_1/repo/api.dart';

class RepoLocations {
  RepoLocations({required this.api});

  final Api api;

  Future<ResultRepoLocations> filter(String name) async {
    try {
      final result =
          await api.dio.get('/location/?name=$name', queryParameters: {});
      final List locationsListJson = result.data['results'] ?? [];
      final locationsList = locationsListJson
          .map(
            (item) => Location.fromJson(item),
          )
          .toList();
      return ResultRepoLocations(locationsList: locationsList);
    } catch (error) {
      print('🏐 Error : $error');
      return ResultRepoLocations(
        errorMessage: S.current.somethingWentWrong,
      );
    }
  }
}

class ResultRepoLocations {
  ResultRepoLocations({
    this.errorMessage,
    this.locationsList,
  });

  final String? errorMessage;
  final List<Location>? locationsList;
}
