import 'package:flutter_app_1/dto/person.dart';
import 'package:flutter_app_1/repo/api.dart';

import '../generated/l10n.dart';

class RepoDetail {
  RepoDetail({required this.api});

  final Api api;

  Future<PersonDetail> getPersonDetail(int id) async {
    try {
      final result = await api.dio.get(
        '/character/$id',
      );

      return PersonDetail(person: Person.fromJson(result.data));
    } catch (error) {
      // ignore: avoid_print
      print('🏐 Error : $error');
      return PersonDetail(
        errorMessage: S.current.somethingWentWrong,
      );
    }
  }
}

class PersonDetail {
  PersonDetail({
    this.errorMessage,
    this.person,
  });

  final String? errorMessage;
  final Person? person;
}
