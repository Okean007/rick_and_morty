// ignore_for_file: avoid_print

import 'package:flutter_app_1/dto/characters_response/characters_response.dart';
import 'package:flutter_app_1/dto/person.dart';
import 'package:flutter_app_1/generated/l10n.dart';
import 'package:flutter_app_1/repo/api.dart';

class RepoPersons {
  RepoPersons({required this.api});

  final Api api;

  Future<ResultRepoPersons> filterByName(String name) async {
    try {
      final result = await api.dio.get(
        '/character/',
        queryParameters: {
          "name": name,
        },
      );
      final List personsListJson = result.data['results'] ?? [];
      final personsList = personsListJson
          .map(
            (item) => Person.fromJson(item),
          )
          .toList();
      return ResultRepoPersons(personsList: personsList);
    } catch (error) {
      print('🏐 Error : $error');
      return ResultRepoPersons(
        errorMessage: S.current.somethingWentWrong,
      );
    }
  }

  Future<CharactersResponse> getPersonse({int? page}) async {
    try {
      final result = await api.dio.get(
        '/character/',
        queryParameters: {
          "page": page ?? 1,
        },
      );

      return CharactersResponse.fromJson(result.data);
    } catch (error) {
      rethrow;
    }
  }

  Future<CharactersResponse> getFiltered({required String filter}) async {
    try {
      final result = await api.dio.get(
        '/character/',
        queryParameters: {
          "status": filter,
        },
      );

      return CharactersResponse.fromJson(result.data);
    } catch (error) {
      rethrow;
    }
  }
}

class ResultRepoPersons {
  ResultRepoPersons({
    this.errorMessage,
    this.personsList,
  });

  final String? errorMessage;
  final List<Person>? personsList;
}
