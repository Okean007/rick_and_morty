import 'package:flutter_app_1/dto/person.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

import 'info.dart';

part 'characters_response.freezed.dart';
part 'characters_response.g.dart';

@freezed
class CharactersResponse with _$CharactersResponse {
  factory CharactersResponse({
    Info? info,
    List<Person>? results,
  }) = _CharactersResponse;

  factory CharactersResponse.fromJson(Map<String, dynamic> json) =>
      _$CharactersResponseFromJson(json);
}
