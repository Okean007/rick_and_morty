// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'characters_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CharactersResponse _$CharactersResponseFromJson(Map<String, dynamic> json) {
  return _CharactersResponse.fromJson(json);
}

/// @nodoc
mixin _$CharactersResponse {
  Info? get info => throw _privateConstructorUsedError;
  List<Person>? get results => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CharactersResponseCopyWith<CharactersResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CharactersResponseCopyWith<$Res> {
  factory $CharactersResponseCopyWith(
          CharactersResponse value, $Res Function(CharactersResponse) then) =
      _$CharactersResponseCopyWithImpl<$Res>;
  $Res call({Info? info, List<Person>? results});

  $InfoCopyWith<$Res>? get info;
}

/// @nodoc
class _$CharactersResponseCopyWithImpl<$Res>
    implements $CharactersResponseCopyWith<$Res> {
  _$CharactersResponseCopyWithImpl(this._value, this._then);

  final CharactersResponse _value;
  // ignore: unused_field
  final $Res Function(CharactersResponse) _then;

  @override
  $Res call({
    Object? info = freezed,
    Object? results = freezed,
  }) {
    return _then(_value.copyWith(
      info: info == freezed
          ? _value.info
          : info // ignore: cast_nullable_to_non_nullable
              as Info?,
      results: results == freezed
          ? _value.results
          : results // ignore: cast_nullable_to_non_nullable
              as List<Person>?,
    ));
  }

  @override
  $InfoCopyWith<$Res>? get info {
    if (_value.info == null) {
      return null;
    }

    return $InfoCopyWith<$Res>(_value.info!, (value) {
      return _then(_value.copyWith(info: value));
    });
  }
}

/// @nodoc
abstract class _$$_CharactersResponseCopyWith<$Res>
    implements $CharactersResponseCopyWith<$Res> {
  factory _$$_CharactersResponseCopyWith(_$_CharactersResponse value,
          $Res Function(_$_CharactersResponse) then) =
      __$$_CharactersResponseCopyWithImpl<$Res>;
  @override
  $Res call({Info? info, List<Person>? results});

  @override
  $InfoCopyWith<$Res>? get info;
}

/// @nodoc
class __$$_CharactersResponseCopyWithImpl<$Res>
    extends _$CharactersResponseCopyWithImpl<$Res>
    implements _$$_CharactersResponseCopyWith<$Res> {
  __$$_CharactersResponseCopyWithImpl(
      _$_CharactersResponse _value, $Res Function(_$_CharactersResponse) _then)
      : super(_value, (v) => _then(v as _$_CharactersResponse));

  @override
  _$_CharactersResponse get _value => super._value as _$_CharactersResponse;

  @override
  $Res call({
    Object? info = freezed,
    Object? results = freezed,
  }) {
    return _then(_$_CharactersResponse(
      info: info == freezed
          ? _value.info
          : info // ignore: cast_nullable_to_non_nullable
              as Info?,
      results: results == freezed
          ? _value._results
          : results // ignore: cast_nullable_to_non_nullable
              as List<Person>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CharactersResponse implements _CharactersResponse {
  _$_CharactersResponse({this.info, final List<Person>? results})
      : _results = results;

  factory _$_CharactersResponse.fromJson(Map<String, dynamic> json) =>
      _$$_CharactersResponseFromJson(json);

  @override
  final Info? info;
  final List<Person>? _results;
  @override
  List<Person>? get results {
    final value = _results;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'CharactersResponse(info: $info, results: $results)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CharactersResponse &&
            const DeepCollectionEquality().equals(other.info, info) &&
            const DeepCollectionEquality().equals(other._results, _results));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(info),
      const DeepCollectionEquality().hash(_results));

  @JsonKey(ignore: true)
  @override
  _$$_CharactersResponseCopyWith<_$_CharactersResponse> get copyWith =>
      __$$_CharactersResponseCopyWithImpl<_$_CharactersResponse>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CharactersResponseToJson(
      this,
    );
  }
}

abstract class _CharactersResponse implements CharactersResponse {
  factory _CharactersResponse({final Info? info, final List<Person>? results}) =
      _$_CharactersResponse;

  factory _CharactersResponse.fromJson(Map<String, dynamic> json) =
      _$_CharactersResponse.fromJson;

  @override
  Info? get info;
  @override
  List<Person>? get results;
  @override
  @JsonKey(ignore: true)
  _$$_CharactersResponseCopyWith<_$_CharactersResponse> get copyWith =>
      throw _privateConstructorUsedError;
}
