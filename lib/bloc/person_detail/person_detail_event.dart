part of 'person_detail_bloc.dart';

abstract class EventBlocDetail {}

class GetDetail extends EventBlocDetail {
  GetDetail(this.id);

  final int id;
}
