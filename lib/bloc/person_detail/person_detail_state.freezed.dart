// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'person_detail_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$PersonDetailState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loadInProgress,
    required TResult Function(Person data) data,
    required TResult Function(String error) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Person data)? data,
    TResult Function(String error)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Person data)? data,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PersonDetailInitial value) initial,
    required TResult Function(PersonDetailLoading value) loadInProgress,
    required TResult Function(PersonDetailData value) data,
    required TResult Function(PersonDetailError value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PersonDetailInitial value)? initial,
    TResult Function(PersonDetailLoading value)? loadInProgress,
    TResult Function(PersonDetailData value)? data,
    TResult Function(PersonDetailError value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PersonDetailInitial value)? initial,
    TResult Function(PersonDetailLoading value)? loadInProgress,
    TResult Function(PersonDetailData value)? data,
    TResult Function(PersonDetailError value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PersonDetailStateCopyWith<$Res> {
  factory $PersonDetailStateCopyWith(
          PersonDetailState value, $Res Function(PersonDetailState) then) =
      _$PersonDetailStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$PersonDetailStateCopyWithImpl<$Res>
    implements $PersonDetailStateCopyWith<$Res> {
  _$PersonDetailStateCopyWithImpl(this._value, this._then);

  final PersonDetailState _value;
  // ignore: unused_field
  final $Res Function(PersonDetailState) _then;
}

/// @nodoc
abstract class _$$PersonDetailInitialCopyWith<$Res> {
  factory _$$PersonDetailInitialCopyWith(_$PersonDetailInitial value,
          $Res Function(_$PersonDetailInitial) then) =
      __$$PersonDetailInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$PersonDetailInitialCopyWithImpl<$Res>
    extends _$PersonDetailStateCopyWithImpl<$Res>
    implements _$$PersonDetailInitialCopyWith<$Res> {
  __$$PersonDetailInitialCopyWithImpl(
      _$PersonDetailInitial _value, $Res Function(_$PersonDetailInitial) _then)
      : super(_value, (v) => _then(v as _$PersonDetailInitial));

  @override
  _$PersonDetailInitial get _value => super._value as _$PersonDetailInitial;
}

/// @nodoc

class _$PersonDetailInitial implements PersonDetailInitial {
  const _$PersonDetailInitial();

  @override
  String toString() {
    return 'PersonDetailState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$PersonDetailInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loadInProgress,
    required TResult Function(Person data) data,
    required TResult Function(String error) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Person data)? data,
    TResult Function(String error)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Person data)? data,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PersonDetailInitial value) initial,
    required TResult Function(PersonDetailLoading value) loadInProgress,
    required TResult Function(PersonDetailData value) data,
    required TResult Function(PersonDetailError value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PersonDetailInitial value)? initial,
    TResult Function(PersonDetailLoading value)? loadInProgress,
    TResult Function(PersonDetailData value)? data,
    TResult Function(PersonDetailError value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PersonDetailInitial value)? initial,
    TResult Function(PersonDetailLoading value)? loadInProgress,
    TResult Function(PersonDetailData value)? data,
    TResult Function(PersonDetailError value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class PersonDetailInitial implements PersonDetailState {
  const factory PersonDetailInitial() = _$PersonDetailInitial;
}

/// @nodoc
abstract class _$$PersonDetailLoadingCopyWith<$Res> {
  factory _$$PersonDetailLoadingCopyWith(_$PersonDetailLoading value,
          $Res Function(_$PersonDetailLoading) then) =
      __$$PersonDetailLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$PersonDetailLoadingCopyWithImpl<$Res>
    extends _$PersonDetailStateCopyWithImpl<$Res>
    implements _$$PersonDetailLoadingCopyWith<$Res> {
  __$$PersonDetailLoadingCopyWithImpl(
      _$PersonDetailLoading _value, $Res Function(_$PersonDetailLoading) _then)
      : super(_value, (v) => _then(v as _$PersonDetailLoading));

  @override
  _$PersonDetailLoading get _value => super._value as _$PersonDetailLoading;
}

/// @nodoc

class _$PersonDetailLoading implements PersonDetailLoading {
  const _$PersonDetailLoading();

  @override
  String toString() {
    return 'PersonDetailState.loadInProgress()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$PersonDetailLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loadInProgress,
    required TResult Function(Person data) data,
    required TResult Function(String error) error,
  }) {
    return loadInProgress();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Person data)? data,
    TResult Function(String error)? error,
  }) {
    return loadInProgress?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Person data)? data,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (loadInProgress != null) {
      return loadInProgress();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PersonDetailInitial value) initial,
    required TResult Function(PersonDetailLoading value) loadInProgress,
    required TResult Function(PersonDetailData value) data,
    required TResult Function(PersonDetailError value) error,
  }) {
    return loadInProgress(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PersonDetailInitial value)? initial,
    TResult Function(PersonDetailLoading value)? loadInProgress,
    TResult Function(PersonDetailData value)? data,
    TResult Function(PersonDetailError value)? error,
  }) {
    return loadInProgress?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PersonDetailInitial value)? initial,
    TResult Function(PersonDetailLoading value)? loadInProgress,
    TResult Function(PersonDetailData value)? data,
    TResult Function(PersonDetailError value)? error,
    required TResult orElse(),
  }) {
    if (loadInProgress != null) {
      return loadInProgress(this);
    }
    return orElse();
  }
}

abstract class PersonDetailLoading implements PersonDetailState {
  const factory PersonDetailLoading() = _$PersonDetailLoading;
}

/// @nodoc
abstract class _$$PersonDetailDataCopyWith<$Res> {
  factory _$$PersonDetailDataCopyWith(
          _$PersonDetailData value, $Res Function(_$PersonDetailData) then) =
      __$$PersonDetailDataCopyWithImpl<$Res>;
  $Res call({Person data});

  $PersonCopyWith<$Res> get data;
}

/// @nodoc
class __$$PersonDetailDataCopyWithImpl<$Res>
    extends _$PersonDetailStateCopyWithImpl<$Res>
    implements _$$PersonDetailDataCopyWith<$Res> {
  __$$PersonDetailDataCopyWithImpl(
      _$PersonDetailData _value, $Res Function(_$PersonDetailData) _then)
      : super(_value, (v) => _then(v as _$PersonDetailData));

  @override
  _$PersonDetailData get _value => super._value as _$PersonDetailData;

  @override
  $Res call({
    Object? data = freezed,
  }) {
    return _then(_$PersonDetailData(
      data: data == freezed
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as Person,
    ));
  }

  @override
  $PersonCopyWith<$Res> get data {
    return $PersonCopyWith<$Res>(_value.data, (value) {
      return _then(_value.copyWith(data: value));
    });
  }
}

/// @nodoc

class _$PersonDetailData implements PersonDetailData {
  const _$PersonDetailData({required this.data});

  @override
  final Person data;

  @override
  String toString() {
    return 'PersonDetailState.data(data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PersonDetailData &&
            const DeepCollectionEquality().equals(other.data, data));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(data));

  @JsonKey(ignore: true)
  @override
  _$$PersonDetailDataCopyWith<_$PersonDetailData> get copyWith =>
      __$$PersonDetailDataCopyWithImpl<_$PersonDetailData>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loadInProgress,
    required TResult Function(Person data) data,
    required TResult Function(String error) error,
  }) {
    return data(this.data);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Person data)? data,
    TResult Function(String error)? error,
  }) {
    return data?.call(this.data);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Person data)? data,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this.data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PersonDetailInitial value) initial,
    required TResult Function(PersonDetailLoading value) loadInProgress,
    required TResult Function(PersonDetailData value) data,
    required TResult Function(PersonDetailError value) error,
  }) {
    return data(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PersonDetailInitial value)? initial,
    TResult Function(PersonDetailLoading value)? loadInProgress,
    TResult Function(PersonDetailData value)? data,
    TResult Function(PersonDetailError value)? error,
  }) {
    return data?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PersonDetailInitial value)? initial,
    TResult Function(PersonDetailLoading value)? loadInProgress,
    TResult Function(PersonDetailData value)? data,
    TResult Function(PersonDetailError value)? error,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this);
    }
    return orElse();
  }
}

abstract class PersonDetailData implements PersonDetailState {
  const factory PersonDetailData({required final Person data}) =
      _$PersonDetailData;

  Person get data;
  @JsonKey(ignore: true)
  _$$PersonDetailDataCopyWith<_$PersonDetailData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$PersonDetailErrorCopyWith<$Res> {
  factory _$$PersonDetailErrorCopyWith(
          _$PersonDetailError value, $Res Function(_$PersonDetailError) then) =
      __$$PersonDetailErrorCopyWithImpl<$Res>;
  $Res call({String error});
}

/// @nodoc
class __$$PersonDetailErrorCopyWithImpl<$Res>
    extends _$PersonDetailStateCopyWithImpl<$Res>
    implements _$$PersonDetailErrorCopyWith<$Res> {
  __$$PersonDetailErrorCopyWithImpl(
      _$PersonDetailError _value, $Res Function(_$PersonDetailError) _then)
      : super(_value, (v) => _then(v as _$PersonDetailError));

  @override
  _$PersonDetailError get _value => super._value as _$PersonDetailError;

  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$PersonDetailError(
      error == freezed
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$PersonDetailError implements PersonDetailError {
  const _$PersonDetailError(this.error);

  @override
  final String error;

  @override
  String toString() {
    return 'PersonDetailState.error(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$PersonDetailError &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  _$$PersonDetailErrorCopyWith<_$PersonDetailError> get copyWith =>
      __$$PersonDetailErrorCopyWithImpl<_$PersonDetailError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loadInProgress,
    required TResult Function(Person data) data,
    required TResult Function(String error) error,
  }) {
    return error(this.error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Person data)? data,
    TResult Function(String error)? error,
  }) {
    return error?.call(this.error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loadInProgress,
    TResult Function(Person data)? data,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this.error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(PersonDetailInitial value) initial,
    required TResult Function(PersonDetailLoading value) loadInProgress,
    required TResult Function(PersonDetailData value) data,
    required TResult Function(PersonDetailError value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(PersonDetailInitial value)? initial,
    TResult Function(PersonDetailLoading value)? loadInProgress,
    TResult Function(PersonDetailData value)? data,
    TResult Function(PersonDetailError value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(PersonDetailInitial value)? initial,
    TResult Function(PersonDetailLoading value)? loadInProgress,
    TResult Function(PersonDetailData value)? data,
    TResult Function(PersonDetailError value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class PersonDetailError implements PersonDetailState {
  const factory PersonDetailError(final String error) = _$PersonDetailError;

  String get error;
  @JsonKey(ignore: true)
  _$$PersonDetailErrorCopyWith<_$PersonDetailError> get copyWith =>
      throw _privateConstructorUsedError;
}
