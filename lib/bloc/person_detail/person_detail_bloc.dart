import 'package:flutter_app_1/bloc/person_detail/person_detail_state.dart';
import 'package:flutter_app_1/repo/repo_details.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'person_detail_event.dart';

class PersonDetailBloc extends Bloc<EventBlocDetail, PersonDetailState> {
  PersonDetailBloc({required this.repo})
      : super(const PersonDetailState.initial()) {
    on<GetDetail>(_getDetailState);
  }
  final RepoDetail repo;

  Future _getDetailState(
    GetDetail event,
    Emitter<PersonDetailState> emit,
  ) async {
    try {
      emit(const PersonDetailState.loadInProgress());
      final result = await repo.getPersonDetail(event.id);
      if (result.errorMessage != null) {
        emit(PersonDetailState.error(result.errorMessage!));
      } else {
        emit(PersonDetailState.data(data: result.person!));
      }
    } catch (e) {
      emit(PersonDetailState.error(e.toString()));
    }
  }
}
