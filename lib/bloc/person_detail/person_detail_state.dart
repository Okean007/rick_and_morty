import 'package:flutter_app_1/dto/person.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'person_detail_state.freezed.dart';

@freezed
class PersonDetailState with _$PersonDetailState {
  const factory PersonDetailState.initial() = PersonDetailInitial;
  const factory PersonDetailState.loadInProgress() = PersonDetailLoading;
  const factory PersonDetailState.data({required Person data}) =
      PersonDetailData;
  const factory PersonDetailState.error(String error) = PersonDetailError;
}
