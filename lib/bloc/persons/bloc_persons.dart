import 'package:flutter_app_1/dto/characters_response/info.dart';
import 'package:flutter_app_1/dto/person.dart';
import 'package:flutter_app_1/repo/repo_persons.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'states.dart';

part 'events.dart';

class BlocPersons extends Bloc<EventBlocPersons, StateBlocPersons> {
  BlocPersons({
    required this.repo,
  }) : super(const StateBlocPersons.initial()) {
    on<EventGetPersons>(_getPersonToState);
    on<EventPersonsSearch>(_getSearchName);
    on<EventPersonFiltered>(_getFiltered);
  }

  final RepoPersons repo;
  late Info _info;
  bool _isLoading = false;
  // bool _isPrev = false;
  bool _isNext = true;
  int _page = 0;
  final List<Person>? _personList = [];

  Future _getPersonToState(
    EventGetPersons event,
    Emitter<StateBlocPersons> emit,
  ) async {
    try {
      // emit(const StateBlocPersons.loading());
      _isLoading = true;
      _page += 1;
      final result = await repo.getPersonse(page: _page);
      _info = result.info!;
      // _isPrev = _info.prev != null;
      _isNext = _info.next != null;
      _isLoading = false;
      _personList!.addAll(result.results!);
      emit(
        StateBlocPersons.data(
          data: _personList!,
          info: _info,
          isLoading: _isLoading,
          isNext: _isNext,
          // isPrev: _isPrev,
        ),
      );
    } catch (e) {
      _page -= 1;
      _isLoading = false;
      emit(StateBlocPersons.error(e.toString()));
    }
  }

  Future _getFiltered(
      EventPersonFiltered event, Emitter<StateBlocPersons> emit) async {
    final result = await repo.getFiltered(filter: event.filter);
    emit(StateBlocPersons.filtered(filtered: result.results!));
  }

  Future _getSearchName(
    EventPersonsSearch event,
    Emitter<StateBlocPersons> emit,
  ) async {
    emit(const StateBlocPersons.loading());
    if (event.name.isEmpty) {
      emit(
        StateBlocPersons.data(
          data: _personList!,
          info: _info,
          isLoading: _isLoading,
          isNext: _isNext,
          // isPrev: _isPrev,
        ),
      );
    } else {
      final result = await repo.filterByName(event.name);
      if (result.errorMessage != null) {
        emit(
          StateBlocPersons.error(result.errorMessage!),
        );
      } else {
        emit(
          StateBlocPersons.findResault(
            data: result.personsList!,
          ),
        );
      }
    }
  }
}
