part of 'bloc_persons.dart';

abstract class EventBlocPersons {}

class EventGetPersons extends EventBlocPersons {}

class EventPersonsSearch extends EventBlocPersons {
  EventPersonsSearch(this.name);

  final String name;
}

class EventPersonFiltered extends EventBlocPersons {
  EventPersonFiltered(this.filter);
  final String filter;
}
