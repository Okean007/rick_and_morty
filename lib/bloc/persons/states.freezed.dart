// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'states.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$StateBlocPersons {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Person> filtered) filtered,
    required TResult Function(
            List<Person> data, Info info, bool isLoading, bool isNext)
        data,
    required TResult Function(List<Person> data) findResault,
    required TResult Function(String error) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Person> filtered)? filtered,
    TResult Function(List<Person> data, Info info, bool isLoading, bool isNext)?
        data,
    TResult Function(List<Person> data)? findResault,
    TResult Function(String error)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Person> filtered)? filtered,
    TResult Function(List<Person> data, Info info, bool isLoading, bool isNext)?
        data,
    TResult Function(List<Person> data)? findResault,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StatePersonsInitial value) initial,
    required TResult Function(StatePersonsLoading value) loading,
    required TResult Function(StatePersonsFiltered value) filtered,
    required TResult Function(StatePersonsData value) data,
    required TResult Function(StatePersonsFindResult value) findResault,
    required TResult Function(StatePersonsError value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StatePersonsInitial value)? initial,
    TResult Function(StatePersonsLoading value)? loading,
    TResult Function(StatePersonsFiltered value)? filtered,
    TResult Function(StatePersonsData value)? data,
    TResult Function(StatePersonsFindResult value)? findResault,
    TResult Function(StatePersonsError value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StatePersonsInitial value)? initial,
    TResult Function(StatePersonsLoading value)? loading,
    TResult Function(StatePersonsFiltered value)? filtered,
    TResult Function(StatePersonsData value)? data,
    TResult Function(StatePersonsFindResult value)? findResault,
    TResult Function(StatePersonsError value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $StateBlocPersonsCopyWith<$Res> {
  factory $StateBlocPersonsCopyWith(
          StateBlocPersons value, $Res Function(StateBlocPersons) then) =
      _$StateBlocPersonsCopyWithImpl<$Res>;
}

/// @nodoc
class _$StateBlocPersonsCopyWithImpl<$Res>
    implements $StateBlocPersonsCopyWith<$Res> {
  _$StateBlocPersonsCopyWithImpl(this._value, this._then);

  final StateBlocPersons _value;
  // ignore: unused_field
  final $Res Function(StateBlocPersons) _then;
}

/// @nodoc
abstract class _$$StatePersonsInitialCopyWith<$Res> {
  factory _$$StatePersonsInitialCopyWith(_$StatePersonsInitial value,
          $Res Function(_$StatePersonsInitial) then) =
      __$$StatePersonsInitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$StatePersonsInitialCopyWithImpl<$Res>
    extends _$StateBlocPersonsCopyWithImpl<$Res>
    implements _$$StatePersonsInitialCopyWith<$Res> {
  __$$StatePersonsInitialCopyWithImpl(
      _$StatePersonsInitial _value, $Res Function(_$StatePersonsInitial) _then)
      : super(_value, (v) => _then(v as _$StatePersonsInitial));

  @override
  _$StatePersonsInitial get _value => super._value as _$StatePersonsInitial;
}

/// @nodoc

class _$StatePersonsInitial
    with DiagnosticableTreeMixin
    implements StatePersonsInitial {
  const _$StatePersonsInitial();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'StateBlocPersons.initial()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'StateBlocPersons.initial'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$StatePersonsInitial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Person> filtered) filtered,
    required TResult Function(
            List<Person> data, Info info, bool isLoading, bool isNext)
        data,
    required TResult Function(List<Person> data) findResault,
    required TResult Function(String error) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Person> filtered)? filtered,
    TResult Function(List<Person> data, Info info, bool isLoading, bool isNext)?
        data,
    TResult Function(List<Person> data)? findResault,
    TResult Function(String error)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Person> filtered)? filtered,
    TResult Function(List<Person> data, Info info, bool isLoading, bool isNext)?
        data,
    TResult Function(List<Person> data)? findResault,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StatePersonsInitial value) initial,
    required TResult Function(StatePersonsLoading value) loading,
    required TResult Function(StatePersonsFiltered value) filtered,
    required TResult Function(StatePersonsData value) data,
    required TResult Function(StatePersonsFindResult value) findResault,
    required TResult Function(StatePersonsError value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StatePersonsInitial value)? initial,
    TResult Function(StatePersonsLoading value)? loading,
    TResult Function(StatePersonsFiltered value)? filtered,
    TResult Function(StatePersonsData value)? data,
    TResult Function(StatePersonsFindResult value)? findResault,
    TResult Function(StatePersonsError value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StatePersonsInitial value)? initial,
    TResult Function(StatePersonsLoading value)? loading,
    TResult Function(StatePersonsFiltered value)? filtered,
    TResult Function(StatePersonsData value)? data,
    TResult Function(StatePersonsFindResult value)? findResault,
    TResult Function(StatePersonsError value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class StatePersonsInitial implements StateBlocPersons {
  const factory StatePersonsInitial() = _$StatePersonsInitial;
}

/// @nodoc
abstract class _$$StatePersonsLoadingCopyWith<$Res> {
  factory _$$StatePersonsLoadingCopyWith(_$StatePersonsLoading value,
          $Res Function(_$StatePersonsLoading) then) =
      __$$StatePersonsLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$StatePersonsLoadingCopyWithImpl<$Res>
    extends _$StateBlocPersonsCopyWithImpl<$Res>
    implements _$$StatePersonsLoadingCopyWith<$Res> {
  __$$StatePersonsLoadingCopyWithImpl(
      _$StatePersonsLoading _value, $Res Function(_$StatePersonsLoading) _then)
      : super(_value, (v) => _then(v as _$StatePersonsLoading));

  @override
  _$StatePersonsLoading get _value => super._value as _$StatePersonsLoading;
}

/// @nodoc

class _$StatePersonsLoading
    with DiagnosticableTreeMixin
    implements StatePersonsLoading {
  const _$StatePersonsLoading();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'StateBlocPersons.loading()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'StateBlocPersons.loading'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$StatePersonsLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Person> filtered) filtered,
    required TResult Function(
            List<Person> data, Info info, bool isLoading, bool isNext)
        data,
    required TResult Function(List<Person> data) findResault,
    required TResult Function(String error) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Person> filtered)? filtered,
    TResult Function(List<Person> data, Info info, bool isLoading, bool isNext)?
        data,
    TResult Function(List<Person> data)? findResault,
    TResult Function(String error)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Person> filtered)? filtered,
    TResult Function(List<Person> data, Info info, bool isLoading, bool isNext)?
        data,
    TResult Function(List<Person> data)? findResault,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StatePersonsInitial value) initial,
    required TResult Function(StatePersonsLoading value) loading,
    required TResult Function(StatePersonsFiltered value) filtered,
    required TResult Function(StatePersonsData value) data,
    required TResult Function(StatePersonsFindResult value) findResault,
    required TResult Function(StatePersonsError value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StatePersonsInitial value)? initial,
    TResult Function(StatePersonsLoading value)? loading,
    TResult Function(StatePersonsFiltered value)? filtered,
    TResult Function(StatePersonsData value)? data,
    TResult Function(StatePersonsFindResult value)? findResault,
    TResult Function(StatePersonsError value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StatePersonsInitial value)? initial,
    TResult Function(StatePersonsLoading value)? loading,
    TResult Function(StatePersonsFiltered value)? filtered,
    TResult Function(StatePersonsData value)? data,
    TResult Function(StatePersonsFindResult value)? findResault,
    TResult Function(StatePersonsError value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class StatePersonsLoading implements StateBlocPersons {
  const factory StatePersonsLoading() = _$StatePersonsLoading;
}

/// @nodoc
abstract class _$$StatePersonsFilteredCopyWith<$Res> {
  factory _$$StatePersonsFilteredCopyWith(_$StatePersonsFiltered value,
          $Res Function(_$StatePersonsFiltered) then) =
      __$$StatePersonsFilteredCopyWithImpl<$Res>;
  $Res call({List<Person> filtered});
}

/// @nodoc
class __$$StatePersonsFilteredCopyWithImpl<$Res>
    extends _$StateBlocPersonsCopyWithImpl<$Res>
    implements _$$StatePersonsFilteredCopyWith<$Res> {
  __$$StatePersonsFilteredCopyWithImpl(_$StatePersonsFiltered _value,
      $Res Function(_$StatePersonsFiltered) _then)
      : super(_value, (v) => _then(v as _$StatePersonsFiltered));

  @override
  _$StatePersonsFiltered get _value => super._value as _$StatePersonsFiltered;

  @override
  $Res call({
    Object? filtered = freezed,
  }) {
    return _then(_$StatePersonsFiltered(
      filtered: filtered == freezed
          ? _value._filtered
          : filtered // ignore: cast_nullable_to_non_nullable
              as List<Person>,
    ));
  }
}

/// @nodoc

class _$StatePersonsFiltered
    with DiagnosticableTreeMixin
    implements StatePersonsFiltered {
  const _$StatePersonsFiltered({required final List<Person> filtered})
      : _filtered = filtered;

  final List<Person> _filtered;
  @override
  List<Person> get filtered {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_filtered);
  }

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'StateBlocPersons.filtered(filtered: $filtered)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'StateBlocPersons.filtered'))
      ..add(DiagnosticsProperty('filtered', filtered));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$StatePersonsFiltered &&
            const DeepCollectionEquality().equals(other._filtered, _filtered));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_filtered));

  @JsonKey(ignore: true)
  @override
  _$$StatePersonsFilteredCopyWith<_$StatePersonsFiltered> get copyWith =>
      __$$StatePersonsFilteredCopyWithImpl<_$StatePersonsFiltered>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Person> filtered) filtered,
    required TResult Function(
            List<Person> data, Info info, bool isLoading, bool isNext)
        data,
    required TResult Function(List<Person> data) findResault,
    required TResult Function(String error) error,
  }) {
    return filtered(this.filtered);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Person> filtered)? filtered,
    TResult Function(List<Person> data, Info info, bool isLoading, bool isNext)?
        data,
    TResult Function(List<Person> data)? findResault,
    TResult Function(String error)? error,
  }) {
    return filtered?.call(this.filtered);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Person> filtered)? filtered,
    TResult Function(List<Person> data, Info info, bool isLoading, bool isNext)?
        data,
    TResult Function(List<Person> data)? findResault,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (filtered != null) {
      return filtered(this.filtered);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StatePersonsInitial value) initial,
    required TResult Function(StatePersonsLoading value) loading,
    required TResult Function(StatePersonsFiltered value) filtered,
    required TResult Function(StatePersonsData value) data,
    required TResult Function(StatePersonsFindResult value) findResault,
    required TResult Function(StatePersonsError value) error,
  }) {
    return filtered(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StatePersonsInitial value)? initial,
    TResult Function(StatePersonsLoading value)? loading,
    TResult Function(StatePersonsFiltered value)? filtered,
    TResult Function(StatePersonsData value)? data,
    TResult Function(StatePersonsFindResult value)? findResault,
    TResult Function(StatePersonsError value)? error,
  }) {
    return filtered?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StatePersonsInitial value)? initial,
    TResult Function(StatePersonsLoading value)? loading,
    TResult Function(StatePersonsFiltered value)? filtered,
    TResult Function(StatePersonsData value)? data,
    TResult Function(StatePersonsFindResult value)? findResault,
    TResult Function(StatePersonsError value)? error,
    required TResult orElse(),
  }) {
    if (filtered != null) {
      return filtered(this);
    }
    return orElse();
  }
}

abstract class StatePersonsFiltered implements StateBlocPersons {
  const factory StatePersonsFiltered({required final List<Person> filtered}) =
      _$StatePersonsFiltered;

  List<Person> get filtered;
  @JsonKey(ignore: true)
  _$$StatePersonsFilteredCopyWith<_$StatePersonsFiltered> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$StatePersonsDataCopyWith<$Res> {
  factory _$$StatePersonsDataCopyWith(
          _$StatePersonsData value, $Res Function(_$StatePersonsData) then) =
      __$$StatePersonsDataCopyWithImpl<$Res>;
  $Res call({List<Person> data, Info info, bool isLoading, bool isNext});

  $InfoCopyWith<$Res> get info;
}

/// @nodoc
class __$$StatePersonsDataCopyWithImpl<$Res>
    extends _$StateBlocPersonsCopyWithImpl<$Res>
    implements _$$StatePersonsDataCopyWith<$Res> {
  __$$StatePersonsDataCopyWithImpl(
      _$StatePersonsData _value, $Res Function(_$StatePersonsData) _then)
      : super(_value, (v) => _then(v as _$StatePersonsData));

  @override
  _$StatePersonsData get _value => super._value as _$StatePersonsData;

  @override
  $Res call({
    Object? data = freezed,
    Object? info = freezed,
    Object? isLoading = freezed,
    Object? isNext = freezed,
  }) {
    return _then(_$StatePersonsData(
      data: data == freezed
          ? _value._data
          : data // ignore: cast_nullable_to_non_nullable
              as List<Person>,
      info: info == freezed
          ? _value.info
          : info // ignore: cast_nullable_to_non_nullable
              as Info,
      isLoading: isLoading == freezed
          ? _value.isLoading
          : isLoading // ignore: cast_nullable_to_non_nullable
              as bool,
      isNext: isNext == freezed
          ? _value.isNext
          : isNext // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }

  @override
  $InfoCopyWith<$Res> get info {
    return $InfoCopyWith<$Res>(_value.info, (value) {
      return _then(_value.copyWith(info: value));
    });
  }
}

/// @nodoc

class _$StatePersonsData
    with DiagnosticableTreeMixin
    implements StatePersonsData {
  const _$StatePersonsData(
      {required final List<Person> data,
      required this.info,
      required this.isLoading,
      required this.isNext})
      : _data = data;

  final List<Person> _data;
  @override
  List<Person> get data {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_data);
  }

  @override
  final Info info;
  @override
  final bool isLoading;
// required bool isPrev,
  @override
  final bool isNext;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'StateBlocPersons.data(data: $data, info: $info, isLoading: $isLoading, isNext: $isNext)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'StateBlocPersons.data'))
      ..add(DiagnosticsProperty('data', data))
      ..add(DiagnosticsProperty('info', info))
      ..add(DiagnosticsProperty('isLoading', isLoading))
      ..add(DiagnosticsProperty('isNext', isNext));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$StatePersonsData &&
            const DeepCollectionEquality().equals(other._data, _data) &&
            const DeepCollectionEquality().equals(other.info, info) &&
            const DeepCollectionEquality().equals(other.isLoading, isLoading) &&
            const DeepCollectionEquality().equals(other.isNext, isNext));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_data),
      const DeepCollectionEquality().hash(info),
      const DeepCollectionEquality().hash(isLoading),
      const DeepCollectionEquality().hash(isNext));

  @JsonKey(ignore: true)
  @override
  _$$StatePersonsDataCopyWith<_$StatePersonsData> get copyWith =>
      __$$StatePersonsDataCopyWithImpl<_$StatePersonsData>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Person> filtered) filtered,
    required TResult Function(
            List<Person> data, Info info, bool isLoading, bool isNext)
        data,
    required TResult Function(List<Person> data) findResault,
    required TResult Function(String error) error,
  }) {
    return data(this.data, info, isLoading, isNext);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Person> filtered)? filtered,
    TResult Function(List<Person> data, Info info, bool isLoading, bool isNext)?
        data,
    TResult Function(List<Person> data)? findResault,
    TResult Function(String error)? error,
  }) {
    return data?.call(this.data, info, isLoading, isNext);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Person> filtered)? filtered,
    TResult Function(List<Person> data, Info info, bool isLoading, bool isNext)?
        data,
    TResult Function(List<Person> data)? findResault,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this.data, info, isLoading, isNext);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StatePersonsInitial value) initial,
    required TResult Function(StatePersonsLoading value) loading,
    required TResult Function(StatePersonsFiltered value) filtered,
    required TResult Function(StatePersonsData value) data,
    required TResult Function(StatePersonsFindResult value) findResault,
    required TResult Function(StatePersonsError value) error,
  }) {
    return data(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StatePersonsInitial value)? initial,
    TResult Function(StatePersonsLoading value)? loading,
    TResult Function(StatePersonsFiltered value)? filtered,
    TResult Function(StatePersonsData value)? data,
    TResult Function(StatePersonsFindResult value)? findResault,
    TResult Function(StatePersonsError value)? error,
  }) {
    return data?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StatePersonsInitial value)? initial,
    TResult Function(StatePersonsLoading value)? loading,
    TResult Function(StatePersonsFiltered value)? filtered,
    TResult Function(StatePersonsData value)? data,
    TResult Function(StatePersonsFindResult value)? findResault,
    TResult Function(StatePersonsError value)? error,
    required TResult orElse(),
  }) {
    if (data != null) {
      return data(this);
    }
    return orElse();
  }
}

abstract class StatePersonsData implements StateBlocPersons {
  const factory StatePersonsData(
      {required final List<Person> data,
      required final Info info,
      required final bool isLoading,
      required final bool isNext}) = _$StatePersonsData;

  List<Person> get data;
  Info get info;
  bool get isLoading; // required bool isPrev,
  bool get isNext;
  @JsonKey(ignore: true)
  _$$StatePersonsDataCopyWith<_$StatePersonsData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$StatePersonsFindResultCopyWith<$Res> {
  factory _$$StatePersonsFindResultCopyWith(_$StatePersonsFindResult value,
          $Res Function(_$StatePersonsFindResult) then) =
      __$$StatePersonsFindResultCopyWithImpl<$Res>;
  $Res call({List<Person> data});
}

/// @nodoc
class __$$StatePersonsFindResultCopyWithImpl<$Res>
    extends _$StateBlocPersonsCopyWithImpl<$Res>
    implements _$$StatePersonsFindResultCopyWith<$Res> {
  __$$StatePersonsFindResultCopyWithImpl(_$StatePersonsFindResult _value,
      $Res Function(_$StatePersonsFindResult) _then)
      : super(_value, (v) => _then(v as _$StatePersonsFindResult));

  @override
  _$StatePersonsFindResult get _value =>
      super._value as _$StatePersonsFindResult;

  @override
  $Res call({
    Object? data = freezed,
  }) {
    return _then(_$StatePersonsFindResult(
      data: data == freezed
          ? _value._data
          : data // ignore: cast_nullable_to_non_nullable
              as List<Person>,
    ));
  }
}

/// @nodoc

class _$StatePersonsFindResult
    with DiagnosticableTreeMixin
    implements StatePersonsFindResult {
  const _$StatePersonsFindResult({required final List<Person> data})
      : _data = data;

  final List<Person> _data;
  @override
  List<Person> get data {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_data);
  }

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'StateBlocPersons.findResault(data: $data)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'StateBlocPersons.findResault'))
      ..add(DiagnosticsProperty('data', data));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$StatePersonsFindResult &&
            const DeepCollectionEquality().equals(other._data, _data));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_data));

  @JsonKey(ignore: true)
  @override
  _$$StatePersonsFindResultCopyWith<_$StatePersonsFindResult> get copyWith =>
      __$$StatePersonsFindResultCopyWithImpl<_$StatePersonsFindResult>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Person> filtered) filtered,
    required TResult Function(
            List<Person> data, Info info, bool isLoading, bool isNext)
        data,
    required TResult Function(List<Person> data) findResault,
    required TResult Function(String error) error,
  }) {
    return findResault(this.data);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Person> filtered)? filtered,
    TResult Function(List<Person> data, Info info, bool isLoading, bool isNext)?
        data,
    TResult Function(List<Person> data)? findResault,
    TResult Function(String error)? error,
  }) {
    return findResault?.call(this.data);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Person> filtered)? filtered,
    TResult Function(List<Person> data, Info info, bool isLoading, bool isNext)?
        data,
    TResult Function(List<Person> data)? findResault,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (findResault != null) {
      return findResault(this.data);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StatePersonsInitial value) initial,
    required TResult Function(StatePersonsLoading value) loading,
    required TResult Function(StatePersonsFiltered value) filtered,
    required TResult Function(StatePersonsData value) data,
    required TResult Function(StatePersonsFindResult value) findResault,
    required TResult Function(StatePersonsError value) error,
  }) {
    return findResault(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StatePersonsInitial value)? initial,
    TResult Function(StatePersonsLoading value)? loading,
    TResult Function(StatePersonsFiltered value)? filtered,
    TResult Function(StatePersonsData value)? data,
    TResult Function(StatePersonsFindResult value)? findResault,
    TResult Function(StatePersonsError value)? error,
  }) {
    return findResault?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StatePersonsInitial value)? initial,
    TResult Function(StatePersonsLoading value)? loading,
    TResult Function(StatePersonsFiltered value)? filtered,
    TResult Function(StatePersonsData value)? data,
    TResult Function(StatePersonsFindResult value)? findResault,
    TResult Function(StatePersonsError value)? error,
    required TResult orElse(),
  }) {
    if (findResault != null) {
      return findResault(this);
    }
    return orElse();
  }
}

abstract class StatePersonsFindResult implements StateBlocPersons {
  const factory StatePersonsFindResult({required final List<Person> data}) =
      _$StatePersonsFindResult;

  List<Person> get data;
  @JsonKey(ignore: true)
  _$$StatePersonsFindResultCopyWith<_$StatePersonsFindResult> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$StatePersonsErrorCopyWith<$Res> {
  factory _$$StatePersonsErrorCopyWith(
          _$StatePersonsError value, $Res Function(_$StatePersonsError) then) =
      __$$StatePersonsErrorCopyWithImpl<$Res>;
  $Res call({String error});
}

/// @nodoc
class __$$StatePersonsErrorCopyWithImpl<$Res>
    extends _$StateBlocPersonsCopyWithImpl<$Res>
    implements _$$StatePersonsErrorCopyWith<$Res> {
  __$$StatePersonsErrorCopyWithImpl(
      _$StatePersonsError _value, $Res Function(_$StatePersonsError) _then)
      : super(_value, (v) => _then(v as _$StatePersonsError));

  @override
  _$StatePersonsError get _value => super._value as _$StatePersonsError;

  @override
  $Res call({
    Object? error = freezed,
  }) {
    return _then(_$StatePersonsError(
      error == freezed
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$StatePersonsError
    with DiagnosticableTreeMixin
    implements StatePersonsError {
  const _$StatePersonsError(this.error);

  @override
  final String error;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'StateBlocPersons.error(error: $error)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'StateBlocPersons.error'))
      ..add(DiagnosticsProperty('error', error));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$StatePersonsError &&
            const DeepCollectionEquality().equals(other.error, error));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(error));

  @JsonKey(ignore: true)
  @override
  _$$StatePersonsErrorCopyWith<_$StatePersonsError> get copyWith =>
      __$$StatePersonsErrorCopyWithImpl<_$StatePersonsError>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function(List<Person> filtered) filtered,
    required TResult Function(
            List<Person> data, Info info, bool isLoading, bool isNext)
        data,
    required TResult Function(List<Person> data) findResault,
    required TResult Function(String error) error,
  }) {
    return error(this.error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Person> filtered)? filtered,
    TResult Function(List<Person> data, Info info, bool isLoading, bool isNext)?
        data,
    TResult Function(List<Person> data)? findResault,
    TResult Function(String error)? error,
  }) {
    return error?.call(this.error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function(List<Person> filtered)? filtered,
    TResult Function(List<Person> data, Info info, bool isLoading, bool isNext)?
        data,
    TResult Function(List<Person> data)? findResault,
    TResult Function(String error)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this.error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(StatePersonsInitial value) initial,
    required TResult Function(StatePersonsLoading value) loading,
    required TResult Function(StatePersonsFiltered value) filtered,
    required TResult Function(StatePersonsData value) data,
    required TResult Function(StatePersonsFindResult value) findResault,
    required TResult Function(StatePersonsError value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(StatePersonsInitial value)? initial,
    TResult Function(StatePersonsLoading value)? loading,
    TResult Function(StatePersonsFiltered value)? filtered,
    TResult Function(StatePersonsData value)? data,
    TResult Function(StatePersonsFindResult value)? findResault,
    TResult Function(StatePersonsError value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(StatePersonsInitial value)? initial,
    TResult Function(StatePersonsLoading value)? loading,
    TResult Function(StatePersonsFiltered value)? filtered,
    TResult Function(StatePersonsData value)? data,
    TResult Function(StatePersonsFindResult value)? findResault,
    TResult Function(StatePersonsError value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class StatePersonsError implements StateBlocPersons {
  const factory StatePersonsError(final String error) = _$StatePersonsError;

  String get error;
  @JsonKey(ignore: true)
  _$$StatePersonsErrorCopyWith<_$StatePersonsError> get copyWith =>
      throw _privateConstructorUsedError;
}
