import 'package:flutter/foundation.dart';
import 'package:flutter_app_1/dto/characters_response/info.dart';

import '../../dto/person.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
part 'states.freezed.dart';

@freezed
class StateBlocPersons with _$StateBlocPersons {
  const factory StateBlocPersons.initial() = StatePersonsInitial;
  const factory StateBlocPersons.loading() = StatePersonsLoading;
  const factory StateBlocPersons.filtered({required List<Person> filtered}) =
      StatePersonsFiltered;
  const factory StateBlocPersons.data({
    required List<Person> data,
    required Info info,
    required bool isLoading,
    // required bool isPrev,
    required bool isNext,
  }) = StatePersonsData;
  const factory StateBlocPersons.findResault({
    required List<Person> data,
  }) = StatePersonsFindResult;
  const factory StateBlocPersons.error(String error) = StatePersonsError;
}
