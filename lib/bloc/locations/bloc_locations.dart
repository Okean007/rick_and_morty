import 'package:flutter_app_1/repo/repo_locations.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'states.dart';

part 'events.dart';

class BlocLocations extends Bloc<EventBlocLocations, StateBlocLocations> {
  BlocLocations({
    required this.repo,
  }) : super(const StateBlocLocations.initial()) {
    on<EventLocationsFilterByName>(
      (event, emit) async {
        emit(const StateBlocLocations.loading());
        final result = await repo.filter(event.name);
        if (result.errorMessage != null) {
          emit(
            StateBlocLocations.error(result.errorMessage!),
          );
          return;
        }
        emit(
          StateBlocLocations.data(data: result.locationsList!),
        );
      },
    );
  }

  final RepoLocations repo;
}
