// ignore_for_file: prefer_final_fields

import 'package:flutter_app_1/dto/episode.dart';
import 'package:flutter_app_1/repo/repo_episodes.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'states.dart';

part 'events.dart';
part 'parts/fetch.dart';
part 'parts/next_page.dart';

class BlocEpisodes extends Bloc<EventBlocEpisodes, StateBlocEpisodes> {
  BlocEpisodes({
    required this.repo,
  }) : super(const StateBlocEpisodes.initial()) {
    on<EventEpisodesFetch>(_fetch);
    on<EventEpisodesNextPage>(_nextPage);
  }

  final RepoEpisodes repo;

  int _currentPage = 1;

  //след страницу если текущая последней
  bool _isEndOfData = false;

  //индикатор, что запрос в процессе
  bool _indicatorPage = false;
}
