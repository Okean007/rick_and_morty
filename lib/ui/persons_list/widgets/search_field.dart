import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_1/bloc/persons/bloc_persons.dart';
import 'package:flutter_app_1/constants/app_colors.dart';
import 'package:flutter_app_1/constants/app_styles.dart';
import 'package:flutter_app_1/generated/l10n.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../filter_screen/filter_screen.dart';

class SearchField extends StatelessWidget {
  const SearchField({
    Key? key,
    this.controller,
    this.onChanged,
  }) : super(key: key);
  final TextEditingController? controller;
  final ValueChanged<String>? onChanged;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Row(
        children: [
          Expanded(
            child: TextField(
              controller: controller,
              onChanged: onChanged,
              style: AppStyles.s16w400,
              cursorColor: AppColors.mainText,
              decoration: InputDecoration(
                hintText: S.of(context).findPerson,
                hintStyle: AppStyles.s16w400.copyWith(
                  color: AppColors.neutral2,
                ),
                isDense: true,
                contentPadding: const EdgeInsets.symmetric(
                  vertical: 14.0,
                  horizontal: 18.0,
                ),
                filled: true,
                fillColor: AppColors.neutral1,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(36.0),
                  borderSide: BorderSide.none,
                ),
                prefixIcon: const Icon(
                  Icons.search,
                  size: 26.0,
                  color: AppColors.neutral2,
                ),
                suffixIcon: IconButton(
                  onPressed: () async {
                    final result = await Navigator.of(context).push(
                      CupertinoPageRoute(
                        builder: (context) => const FilterScreen(),
                      ),
                    );
                    if (result != null) {
                      BlocProvider.of<BlocPersons>(context)
                          .add(EventPersonFiltered(result));
                    } else if (result == null && result == '') {
                      BlocProvider.of<BlocPersons>(context)
                          .add(EventGetPersons());
                    }
                  },
                  icon: const Icon(
                    Icons.filter_list_alt,
                    size: 26.0,
                    color: AppColors.neutral2,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
