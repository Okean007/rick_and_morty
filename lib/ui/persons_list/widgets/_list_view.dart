part of '../persons_list_screen.dart';

class _ListView extends StatelessWidget {
  const _ListView({
    Key? key,
    required this.personsList,
    this.controller,
  }) : super(key: key);

  final List<Person> personsList;
  final ScrollController? controller;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      controller: controller,
      padding: const EdgeInsets.only(
        top: 12.0,
        left: 12.0,
        right: 12.0,
      ),
      itemCount: personsList.length,
      itemBuilder: (context, index) {
        return InkWell(
          child: PersonListTile(personsList[index]),
          onTap: () {
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (context) => DetailPerson(id: personsList[index].id!),
              ),
            );
          },
        );
      },
      separatorBuilder: (context, _) => const SizedBox(height: 26.0),
    );
  }
}
