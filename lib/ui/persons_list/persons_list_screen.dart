import 'package:flutter/material.dart';
import 'package:flutter_app_1/bloc/persons/bloc_persons.dart';
import 'package:flutter_app_1/constants/app_colors.dart';
import 'package:flutter_app_1/constants/app_styles.dart';
import 'package:flutter_app_1/dto/person.dart';
import 'package:flutter_app_1/generated/l10n.dart';
import 'package:flutter_app_1/ui/detail_person/detail_person.dart';
import 'package:flutter_app_1/widgets/app_nav_bar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/persons/states.dart';
import 'widgets/person_grid_tile.dart';
import 'widgets/person_list_tile.dart';
import 'widgets/search_field.dart';

part 'widgets/_grid_view.dart';
part 'widgets/_list_view.dart';

class PersonsListScreen extends StatefulWidget {
  const PersonsListScreen({Key? key}) : super(key: key);
  static final isListView = ValueNotifier(true);

  @override
  State<PersonsListScreen> createState() => _PersonsListScreenState();
}

class _PersonsListScreenState extends State<PersonsListScreen> {
  late TextEditingController _searchController;
  late ScrollController _listController;
  late ScrollController _filteredController;

  @override
  void initState() {
    _searchController = TextEditingController();
    _listController = ScrollController();
    _filteredController = ScrollController();

    super.initState();
  }

  @override
  void dispose() {
    _searchController.dispose();
    _filteredController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    bool _isLoading = false;
    bool _isNext = false;

    _listController.addListener(() {
      final _posiAfter = _listController.position.extentAfter;
      // ignore: unused_local_variable
      final _posiBefore = _listController.position.extentBefore;
      if (_posiAfter < 10 && !_isLoading && _isNext) {
        BlocProvider.of<BlocPersons>(context).add(EventGetPersons());
      }
    });
    return ColoredBox(
      color: Theme.of(context).scaffoldBackgroundColor,
      child: SafeArea(
        child: Scaffold(
            bottomNavigationBar: const AppNavBar(current: 0),
            body: Column(
              children: [
                SearchField(
                  controller: _searchController,
                  onChanged: (value) {
                    BlocProvider.of<BlocPersons>(context).add(
                      EventPersonsSearch(value),
                    );
                  },
                ),
                BlocBuilder<BlocPersons, StateBlocPersons>(
                  builder: (context, state) {
                    return state.when(
                      filtered: (filtered) => Expanded(
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 12.0),
                              child: Row(
                                children: [
                                  IconButton(
                                    icon: const Icon(Icons.grid_view),
                                    iconSize: 28.0,
                                    color: AppColors.neutral2,
                                    onPressed: () {
                                      PersonsListScreen.isListView.value =
                                          !PersonsListScreen.isListView.value;
                                    },
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              child: ValueListenableBuilder<bool>(
                                valueListenable: PersonsListScreen.isListView,
                                builder: (context, isListViewMode, _) {
                                  return isListViewMode
                                      ? _ListView(
                                          personsList: filtered,
                                          controller: _filteredController,
                                        )
                                      : _GridView(
                                          personsList: filtered,
                                          controller: _filteredController,
                                        );
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                      findResault: (data) => SearchResultList(
                        personList: data,
                      ),
                      initial: () => const SizedBox.shrink(),
                      loading: () => Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          CircularProgressIndicator(),
                        ],
                      ),
                      data: ((
                        data,
                        info,
                        isLoading,
                        isNext,
                      ) {
                        _isLoading = isLoading;
                        _isNext = isNext;
                        return Expanded(
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 12.0),
                                child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                        S
                                            .of(context)
                                            .personsTotal(info.count!)
                                            .toUpperCase(),
                                        style: AppStyles.s10w500.copyWith(
                                          letterSpacing: 1.5,
                                          color: AppColors.neutral2,
                                        ),
                                      ),
                                    ),
                                    IconButton(
                                      icon: const Icon(Icons.grid_view),
                                      iconSize: 28.0,
                                      color: AppColors.neutral2,
                                      onPressed: () {
                                        PersonsListScreen.isListView.value =
                                            !PersonsListScreen.isListView.value;
                                      },
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: ValueListenableBuilder<bool>(
                                  valueListenable: PersonsListScreen.isListView,
                                  builder: (context, isListViewMode, _) {
                                    return isListViewMode
                                        ? _ListView(
                                            personsList: data,
                                            controller: _listController,
                                          )
                                        : _GridView(
                                            personsList: data,
                                            controller: _listController,
                                          );
                                  },
                                ),
                              ),
                            ],
                          ),
                        );
                      }),
                      error: (error) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Text(error),
                            ),
                          ],
                        );
                      },
                    );
                  },
                ),
              ],
            )),
      ),
    );
  }
}

class SearchResultList extends StatelessWidget {
  const SearchResultList({Key? key, required this.personList})
      : super(key: key);

  final List<Person> personList;
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.separated(
        padding: const EdgeInsets.only(
          top: 12.0,
          left: 12.0,
          right: 12.0,
        ),
        itemCount: personList.length,
        itemBuilder: (context, index) {
          return InkWell(
            child: PersonListTile(personList[index]),
            onTap: () {
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (context) => DetailPerson(id: personList[index].id!),
                ),
              );
            },
          );
        },
        separatorBuilder: (context, _) => const SizedBox(height: 26.0),
      ),
    );
  }
}
