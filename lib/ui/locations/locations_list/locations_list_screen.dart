import 'package:flutter/material.dart';
import 'package:flutter_app_1/bloc/locations/bloc_locations.dart';
import 'package:flutter_app_1/bloc/locations/states.dart';
import 'package:flutter_app_1/constants/app_colors.dart';
import 'package:flutter_app_1/constants/app_styles.dart';
import 'package:flutter_app_1/generated/l10n.dart';
import 'package:flutter_app_1/widgets/app_nav_bar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'widgets/location_list_tile.dart';
import 'widgets/search_field.dart';

class LocationsListScreen extends StatelessWidget {
  const LocationsListScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: Theme.of(context).scaffoldBackgroundColor,
      child: SafeArea(
        child: Scaffold(
          bottomNavigationBar: const AppNavBar(current: 1),
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          body: Column(
            children: [
              SearchField(
                onChanged: (value) {
                  BlocProvider.of<BlocLocations>(context).add(
                    EventLocationsFilterByName(value),
                  );
                },
              ),
              BlocBuilder<BlocLocations, StateBlocLocations>(
                builder: (context, state) {
                  var personsTotal = 0;
                  if (state is StateLocationsData) {
                    personsTotal = state.data.length;
                  }
                  return Padding(
                    padding: const EdgeInsets.fromLTRB(12, 0, 12, 4),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            S
                                .of(context)
                                .locationsTotal(personsTotal)
                                .toUpperCase(),
                            style: AppStyles.s10w500.copyWith(
                              letterSpacing: 1.5,
                              color: AppColors.neutral2,
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
              Expanded(
                child: BlocBuilder<BlocLocations, StateBlocLocations>(
                  builder: (context, state) {
                    return state.when(
                      initial: () => const SizedBox.shrink(),
                      loading: () => Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          CircularProgressIndicator(),
                        ],
                      ),
                      data: (data) {
                        if (data.isEmpty) {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Flexible(
                                child: Text(S.of(context).personsListIsEmpty),
                              ),
                            ],
                          );
                        } else {
                          return NotificationListener(
                            onNotification: (ScrollNotification notification) {
                              final current = notification.metrics.pixels + 100;
                              final max = notification.metrics.maxScrollExtent;
                              if (current >= max) {}

                              return false;
                            },
                            child: ListView.separated(
                              padding: const EdgeInsets.only(
                                top: 12.0,
                                left: 12.0,
                                right: 12.0,
                              ),
                              itemCount: data.length,
                              itemBuilder: (context, index) {
                                return InkWell(
                                  child: LocationListTile(data[index]),
                                  onTap: () {},
                                );
                              },
                              separatorBuilder: (context, _) => const Divider(),
                            ),
                          );
                        }
                      },
                      error: (error) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              child: Text(error),
                            ),
                          ],
                        );
                      },
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
