import 'package:flutter/material.dart';
import 'package:flutter_app_1/constants/app_colors.dart';
import 'package:flutter_app_1/constants/app_styles.dart';
import 'package:flutter_app_1/widgets/app_button_styles.dart';

import '../../generated/l10n.dart';

class FilterScreen extends StatefulWidget {
  const FilterScreen({Key? key}) : super(key: key);

  @override
  State<FilterScreen> createState() => _FilterScreenState();
}

String? status;

class _FilterScreenState extends State<FilterScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          S.of(context).filter,
          style: AppStyles.s20w500,
        ),
        elevation: 0,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back,
            color: AppColors.mainText,
          ),
        ),
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Divider(),
            const SizedBox(
              height: 36,
            ),
            Text(
              S.of(context).status,
              style: AppStyles.s16w450,
            ),
            RadioListTile(
              title: Text(S.of(context).alive),
              value: "alive",
              groupValue: status,
              onChanged: (value) {
                setState(() {
                  status = value.toString();
                });
              },
            ),
            RadioListTile(
              title: Text(S.of(context).dead),
              value: "dead",
              groupValue: status,
              onChanged: (value) {
                setState(() {
                  status = value.toString();
                });
              },
            ),
            RadioListTile(
              title: Text(S.of(context).noData),
              value: "unknown",
              groupValue: status,
              onChanged: (value) {
                setState(() {
                  status = value.toString();
                });
              },
            ),
            RadioListTile(
              title: Text(S.of(context).ofFilter),
              value: '',
              groupValue: status,
              onChanged: (value) {
                setState(() {
                  status = value.toString();
                });
              },
            ),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                  onPressed: () {
                    Navigator.pop<String?>(context, status);
                  },
                  child: Text(S.of(context).ok),
                  style: AppButtonStyles.elevated1),
            ),
          ],
        ),
      ),
    );
  }
}
