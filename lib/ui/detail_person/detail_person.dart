import 'package:flutter/material.dart';
import 'package:flutter_app_1/bloc/person_detail/person_detail_bloc.dart';
import 'package:flutter_app_1/bloc/person_detail/person_detail_state.dart';
import 'package:flutter_app_1/constants/app_colors.dart';
import 'package:flutter_app_1/constants/app_styles.dart';
import 'package:flutter_app_1/dto/person.dart';
import 'package:flutter_app_1/repo/repo_details.dart';
import 'package:flutter_app_1/ui/persons_list/persons_list_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../generated/l10n.dart';

class DetailPerson extends StatelessWidget {
  const DetailPerson({Key? key, required this.id}) : super(key: key);
  final int id;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        create: (context) =>
            PersonDetailBloc(repo: RepositoryProvider.of<RepoDetail>(context))
              ..add(GetDetail(id)),
        child: BlocBuilder<PersonDetailBloc, PersonDetailState>(
          builder: (context, state) {
            return state.when(
              initial: () => const SizedBox.shrink(),
              loadInProgress: () => Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  CircularProgressIndicator(),
                ],
              ),
              data: (data) => DetailSuccess(person: data),
              error: (error) {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Flexible(
                      child: Text(error),
                    ),
                  ],
                );
              },
            );
          },
        ),
      ),
    );
  }
}

class DetailSuccess extends StatelessWidget {
  const DetailSuccess({Key? key, required this.person}) : super(key: key);
  final Person person;
  @override
  Widget build(BuildContext context) {
    Color _statusColor(String? status) {
      if (status == 'Dead') return Colors.red;
      if (status == 'Alive') return const Color(0xff00c48c);
      return Colors.grey;
    }

    String _statusLabel(String? status) {
      if (status == 'Dead') return S.current.dead;
      if (status == 'Alive') return S.current.alive;
      return S.current.noData;
    }

    return Column(
      children: [
        Expanded(
          child: Stack(
            children: [
              Positioned(
                  child: Image.network(person.image ?? '',
                      height: 300, width: double.infinity, fit: BoxFit.fill)),
              Positioned(
                child: Container(
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        AppColors.avatarGradientFirst,
                        AppColors.avatarGradientSecond,
                        AppColors.avatarGradientThree
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 218,
                child: Container(
                  color: AppColors.background,
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16,
                    ),
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 90,
                        ),
                        Text(person.name ?? '', style: AppStyles.s34w400),
                        const SizedBox(
                          height: 10,
                        ),
                        Text(
                          _statusLabel(person.status ?? ''),
                          style: AppStyles.s10w500.copyWith(
                            letterSpacing: 1.5,
                            color: _statusColor(
                              person.status,
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 56,
                        ),
                        Text(
                          S.of(context).history,
                          style: const TextStyle(color: AppColors.mainText),
                        ),
                        const SizedBox(
                          height: 24,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                S.of(context).gender,
                                style:
                                    const TextStyle(color: AppColors.neutral2),
                              ),
                              Text(
                                S.of(context).species,
                                style:
                                    const TextStyle(color: AppColors.neutral2),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                person.gender ?? '',
                                style:
                                    const TextStyle(color: AppColors.mainText),
                              ),
                              Text(
                                person.species ?? '',
                                style:
                                    const TextStyle(color: AppColors.mainText),
                              ),
                            ],
                          ),
                        ),
                        const SizedBox(
                          height: 44,
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            S.of(context).PlaceofBirth,
                            style: const TextStyle(color: AppColors.neutral2),
                          ),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            S.of(context).noData,
                            style: const TextStyle(color: AppColors.mainText),
                          ),
                        ),
                        const SizedBox(
                          height: 44,
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            S.of(context).Location,
                            style: const TextStyle(color: AppColors.neutral2),
                          ),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            S.of(context).noData,
                            style: const TextStyle(color: AppColors.mainText),
                          ),
                        ),
                        const SizedBox(
                          height: 36,
                        ),
                        const Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              S.of(context).episode,
                              style: AppStyles.s20w500,
                            ),
                            Text(
                              S.of(context).allEpisode,
                              style: const TextStyle(color: AppColors.neutral2),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 138,
                left: MediaQuery.of(context).size.width / 2 - 73,
                child: Container(
                  width: 146,
                  height: 146,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: NetworkImage(
                            person.image ?? '',
                          ),
                          fit: BoxFit.cover)),
                ),
              ),
              Positioned(
                height: 90,
                child: IconButton(
                    onPressed: () {
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(
                          builder: (context) => const PersonsListScreen(),
                        ),
                      );
                    },
                    icon: const Icon(
                      Icons.arrow_back,
                      color: AppColors.background,
                    )),
              ),
            ],
          ),
        )
      ],
    );
  }
}
